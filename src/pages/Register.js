import React, { useEffect, useState } from 'react';
import { Container, Form, Button } from 'react-bootstrap';

const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [cpw, setCpw] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        if (email !== '' && password !== '' && cpw !== '' && password === cpw) {
            setIsDisabled(false);
        }
    }, [email, password, cpw]);

    const registerUser = (e) => {
        e.preventDefault();
        setEmail('');
        setPassword('');
        setCpw('');

        alert('Successful Registration');
    };

    return (
        <Container>
            <Form className="border p-3" onSubmit={() => registerUser()}>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="cpw">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={cpw}
                        onChange={(e) => setCpw(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
        </Container>
    );
};

export default Register;
