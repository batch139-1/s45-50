import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsDisabled(false);
        }
    }, [email, password]);

    const loginUser = (e) => {
        e.preventDefault();
        setEmail('');
        setPassword('');

        localStorage.setItem('email', email);

        alert('Successful Login');
    };

    return (
        <Container className="my-5">
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Form className="border p-3" onSubmit={(e) => loginUser(e)}>
                        <h2 className="mb-3">Login</h2>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => {
                                    setEmail(e.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                        >
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicCheckbox"
                        ></Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={loginUser}
                            disabled={isDisabled}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default Login;
