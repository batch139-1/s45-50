import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './components/ErrorPage';

export default function App() {
    return (
        <>
            <Router>
                <AppNavbar />
                <Container fluid>
                    <Switch>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route path="/courses">
                            <Courses />
                        </Route>
                        <Route path="/register">
                            <Register />
                        </Route>
                        <Route path="/login">
                            <Login />
                        </Route>
                        <Route path="*">
                            <ErrorPage />
                        </Route>
                    </Switch>
                </Container>
            </Router>
        </>
    );
}
