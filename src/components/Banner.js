
import {Container, Row, Col, Button} from 'react-bootstrap'

export default function Banner(){

	return(
		<Container fluid>
			<Row>
				<Col>
					<div className="jumbotron">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere</p>
						<Button variant="primary">Enroll Now</Button>
					</div>
				</Col>
			</Row>
		</Container>

	)
}