import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

const ErrorPage = () => {
    return (
        <Container className="my-5">
            <Row>
                <Col xs={10} md={8}>
                    <div>
                        <h1>Error</h1>
                        <p>The page you are looking for cannot be found</p>
                        <p>
                            Click here to go back to <a href="/">homepage</a>
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default ErrorPage;
