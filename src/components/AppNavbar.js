import { useState } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { NavLink, Link } from 'react-router-dom';

export default function AppNavbar() {
    const [user, setUser] = useState(localStorage.getItem('email'));
    const logout = () => {
        localStorage.clear();
        history.push('/login');
    };

    const history = useHistory();

    return (
        <Navbar bg="primary" expand="lg">
            <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link>Home</Nav.Link>
                    <Nav.Link>Link</Nav.Link>
                    {!user && <Nav.Link>Register</Nav.Link>}
                    {!user && <Nav.Link>Login</Nav.Link>}
                    {user && <Nav.Link onClick={logout()}>Logout</Nav.Link>}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
